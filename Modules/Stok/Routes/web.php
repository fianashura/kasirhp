<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('stok')->group(function() {
    Route::get('/', 'StokController@index')->name('stok');
    Route::post('/add', 'StokController@store')->name('stok_add');
    Route::get('/edit/{id_stok}', 'StokController@edit')->name('stok_edit');
    Route::post('/update/{id_stok}', 'StokController@update')->name('stok_update');
    Route::get('/delete/{id_stok}', 'StokController@destroy')->name('stok_delete');
});
