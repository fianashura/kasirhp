@extends('home::layouts.master')

@section('css')
    {{-- <link href="{{ asset('admin/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('admin/assets/assets/sweetalert2/sweetalert2.css') }}"/>
@endsection

@section('content')

    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Daftar Stok</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Daftar Stok</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <button onclick="addForm()" type="button" class="btn btn-primary width-md"><i data-feather="plus-circle"></i> Tambah Data</button><br><br>
                            <div class="table-responsive">
                            <table id="data-stok" class="table dt-responsive nowrap table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Produk</th>
                                        <th width="20%">Harga</th>
                                        <th width="10%">Jumlah</th>
                                        <th width="20%">Foto</th>
                                        <th width="10%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stok as $stk)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$stk->nm_produk}}</td>
                                            <td>Rp. {{number_format($stk->harga)}}</td>
                                            <td>{{$stk->jumlah}}</td>
                                            <td>
                                                <img src="{{asset('foto/produk/'.$stk->foto)}}" style="width: 50%">
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button onclick="editForm('{{$stk->id_stok}}')" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Data"><i data-feather="edit"></i></button>
                                                    <button onclick="deleteData('{{$stk->id_stok}}')" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Data"><i data-feather="trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
    @include('stok::form')
    

@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('admin/assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('admin/assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.select.min.js') }}"></script>
    
    <!-- Datatables init -->
    <script src="{{ asset('admin/assets/js/pages/datatables.init.js') }}"></script>
    
    <script src="{{ asset('admin/assets/assets/sweetalert2/sweetalert2.min.js') }}"></script>
                
    <script type="text/javascript">
        var table = $('#data-stok').DataTable();
    
        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#modal-form').modal('show');
          $('.modal-title').text('Tambah Stok');
          $('#modal-form form')[0].reset();
          
        }
    
        function editForm(id) {
          save_method = 'edit';
          $('input[name=_method]').val('POST');
          $('#modal-form form')[0].reset();
          $.ajax({
            url: "{{ url('stok/edit') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit menu');
    
              $('#id').val(data.id_stok);
              $('#produk').val(data.id_produk);
              $('#jumlah').val(data.jumlah);
              $('#tgl_masuk').val(data.tgl_masuk);
            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }
    
        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
          }).then(function () {
              $.ajax({
                  url : "{{ url('stok/delete') }}" + '/' + id,
                  type : "GET",
                  data : {'_method' : 'GET', '_token' : csrf_token},
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                      })
                      window.location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        }

    
        $(function(){
              $('#modal-form').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      var id = $('#id').val();
                      if (save_method == 'add') url = "{{ url('stok/add/') }}";
                      else url = "{{ url('stok/update') . '/' }}" + id;
    
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#modal-form form').serialize(),
                          data : new FormData($('#modal-form form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#modal-form').modal('hide');
                              swal({
                                  title: 'Success!',
                                  text: data.message,
                                  type: 'success',
                                  timer: '1500'
                              })
                              window.location.reload();
                          },
                          error : function(){
                              swal({
                                  title: 'Oops...',
                                  text: data.message,
                                  type: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });

      </script>
      {{-- <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
      <script>
      var editor = document.getElementById("editor");
          CKEDITOR.replace(editor,{
          language:'en-gb'
      });
      CKEDITOR.config.allowedContent = true;
      
      </script> --}}
@endsection