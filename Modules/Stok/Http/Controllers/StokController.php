<?php

namespace Modules\Stok\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Master\Entities\Produk;
use Modules\Stok\Entities\Stok;
use Illuminate\Support\Str;
use App\User;
use DB;
use DataTables;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $no = 1;
        $stok = Stok::leftjoin('produk','stok.id_produk','produk.id_produk')->get();
        $produk = Produk::get();
        return view('stok::index', compact('no','stok','produk'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('stok::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $stok = new Stok;
        $stok->id_stok = Str::upper(Str::uuid());
        $stok->id_produk = $request['produk'];
        $stok->jumlah = $request['jumlah'];
        $stok->tgl_masuk = $request['tgl_masuk'];

        $stok->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Terinput'
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('stok::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id_stok)
    {
        $stok = stok::where('id_stok',$id_stok)->first();
            // dd($produk);
        return $stok;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id_stok)
    {
        DB::table('stok')->where('id_stok',$id_stok)->update([
            'id_produk' => $request->produk,
            'jumlah' => $request->jumlah,
            'tgl_masuk' => $request->tgl_masuk,
        ]);
    

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id_stok)
    {
        DB::table('stok')->where('id_stok',$id_stok)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }
}
