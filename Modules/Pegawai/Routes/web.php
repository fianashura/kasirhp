<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('pegawai')->group(function() {
    Route::get('/', 'PegawaiController@index')->name('pegawai');
    Route::get('/json', 'PegawaiController@json')->name('pegawai.json');
    Route::post('/save', 'PegawaiController@store')->name('pegawai.save');
    Route::get('/edit/{id_pegawai}', 'PegawaiController@edit')->name('pegawai.edit');
    Route::post('/update/{id_pegawai}', 'PegawaiController@update')->name('pegawai.update');
    Route::get('/delete/{id_pegawai}', 'PegawaiController@destroy')->name('pegawai.delete');
});
