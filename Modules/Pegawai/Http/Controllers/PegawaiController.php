<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Pegawai\Entities\Pegawai;
use Illuminate\Support\Str;
use App\User;
use DB;
use DataTables;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $no = 1;
        $pegawai = DB::table('pegawai')
            ->orderBy('nm_pegawai','ASC')
            ->get();
        return view('pegawai::index', compact('no','pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('pegawai::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $karyawan = new Pegawai;
        $karyawan->id_pegawai = Str::upper(Str::uuid());
        $karyawan->nm_pegawai = $request['nm_pegawai'];
        $karyawan->jabatan = $request['jabatan'];
        $karyawan->no_hp = $request['no_hp'];
        $karyawan->alamat = $request['alamat'];

        $file  = $request->file('foto');
        $exe = $file->getClientOriginalExtension();
        $filename = rand(1,100000).'.'.$exe;
        $request->file('foto')->move("foto/pegawai", $filename);

        $karyawan->foto = $filename;

        $karyawan->save();

        $user = new User;
        $user->id = Str::upper(Str::uuid());
        $user->person_id = $karyawan->id_pegawai;
        $user->role = $request['jabatan'];
        $user->name = $request['nm_pegawai'];
        $user->email = $request['username'];
        $user->password = bcrypt('12345');
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Terinput'
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('pegawai::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id_pegawai)
    {
        $pegawai = pegawai::where('id_pegawai',$id_pegawai)->first();
        return $pegawai;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id_pegawai)
    {
        if($request->foto != NULL){
            $file  = $request->file('foto');
            $exe = $file->getClientOriginalExtension();
            $filename = rand(1,100000).'.'.$exe;
            $request->file('foto')->move("foto/pegawai", $filename);

            DB::table('pegawai')->where('id_pegawai',$id_pegawai)->update([
                'nm_pegawai' => $request->nm_pegawai,
                'jabatan' => $request->jabatan,
                'no_hp' => $request->no_hp,
                'alamat' => $request->alamat,
                'foto' => $filename,
            ]);
        }else{
            DB::table('pegawai')->where('id_pegawai',$id_pegawai)->update([
                'nm_pegawai' => $request->nm_pegawai,
                'jabatan' => $request->jabatan,
                'no_hp' => $request->no_hp,
                'alamat' => $request->alamat,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id_pegawai)
    {
        DB::table('pegawai')->where('id_pegawai',$id_pegawai)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

}
