<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Master\Entities\Produk;
use Illuminate\Support\Str;
use App\User;
use DB;
use DataTables;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $no = 1;
        $produk = DB::table('produk')
            ->orderBy('nm_produk','ASC')
            ->get();
        return view('master::index', compact('produk','no'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('master::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $produk = new Produk;
        $produk->id_produk = Str::upper(Str::uuid());
        $produk->nm_produk = $request['nm_produk'];
        $produk->harga = $request['harga'];

        $file  = $request->file('foto');
        $exe = $file->getClientOriginalExtension();
        $filename = rand(1,100000).'.'.$exe;
        $request->file('foto')->move("foto/produk", $filename);

        $produk->foto = $filename;

        $produk->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Terinput'
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id_produk)
    {
        $produk = Produk::where('id_produk',$id_produk)->first();
            // dd($produk);
        return $produk;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id_produk)
    {
        if($request->foto != NULL){
            $file  = $request->file('foto');
            $exe = $file->getClientOriginalExtension();
            $filename = rand(1,100000).'.'.$exe;
            $request->file('foto')->move("foto/produk", $filename);

            DB::table('produk')->where('id_produk',$id_produk)->update([
                'nm_produk' => $request->nm_produk,
                'harga' => $request->harga,
                'foto' => $filename,
            ]);
        }else{
            DB::table('produk')->where('id_produk',$id_produk)->update([
                'nm_produk' => $request->nm_produk,
                'harga' => $request->harga,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id_produk)
    {
        DB::table('produk')->where('id_produk',$id_produk)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }
}
