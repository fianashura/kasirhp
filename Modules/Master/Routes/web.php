<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('master')->group(function() {
    Route::get('/produk', 'MasterController@index')->name('produk');
    Route::post('/produk/add', 'MasterController@store')->name('produk_add');
    Route::get('/produk/edit/{id_produk}', 'MasterController@edit')->name('produk_edit');
    Route::post('/produk/update/{id_produk}', 'MasterController@update')->name('produk_update');
    Route::get('/produk/delete/{id_produk}', 'MasterController@destroy')->name('produk_delete');
});
