@extends('home::layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row page-title align-items-center">
            <div class="col-sm-4 col-xl-6">
                <h4 class="mb-1 mt-0">Dashboard</h4>
            </div>
            <div class="col-sm-8 col-xl-6">
                
            </div>
        </div>

        <!-- content -->
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Hari Ini</span>
                                <h3 class="mb-0">Rp. {{number_format($cust)}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Produk Terjual</span>
                                <h3 class="mb-0">{{$prod}} Produk</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Customer</span>
                                <h3 class="mb-0">{{$customer}} Orang</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Stok</span>
                                <h3 class="mb-0">{{$stok}} Produk</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- stats + charts -->
        <div class="row">
            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <h5 class="card-title header-title border-bottom p-3 mb-0">Overview</h5>
                        <!-- stat 1 -->
                        <div class="media px-3 py-4 border-bottom">
                            <div class="media-body">
                                <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$customer_total}}</h4>
                                <span class="text-muted">Total Customer</span>
                            </div>
                            <i data-feather="users" class="align-self-center icon-dual icon-lg"></i>
                        </div>

                        <!-- stat 2 -->
                        <div class="media px-3 py-4 border-bottom">
                            <div class="media-body">
                                <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$produk_total}}</h4>
                                <span class="text-muted">Total Produk Terjual</span>
                            </div>
                            <i data-feather="image" class="align-self-center icon-dual icon-lg"></i>
                        </div>

                        <!-- stat 3 -->
                        <div class="media px-3 py-4">
                            <div class="media-body">
                                <h5 class="mt-0 mb-1 font-size-18 font-weight-normal">Rp. {{number_format($hasil_total)}}</h5>
                                <span class="text-muted">Total Penghasilan</span>
                            </div>
                            <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-9">
                <div class="card">
                    <div class="card-body pb-0">
                        <h5 class="card-title mb-0 header-title">Revenue</h5>

                        <div id="revenue-chart" class="apex-charts mt-3"  dir="ltr"></div>
                    </div>
                </div>
            </div>

        </div>
        <!-- row -->

        <!-- products -->
        <div class="row">
            <div class="col-xl-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-0 header-title">Sales By Category</h5>
                        <div id="sales-by-category-chart" class="apex-charts mb-0 mt-4" dir="ltr"></div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->
            <div class="col-xl-7">
                <div class="card">
                    <div class="card-body">
                        <a href="#" class="btn btn-primary btn-sm float-right">
                            <i class='uil uil-export ml-1'></i> Export
                        </a>
                        <h5 class="card-title mt-0 mb-0 header-title">Recent Orders</h5>

                        <div class="table-responsive mt-4">
                            <table class="table table-hover table-nowrap mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">No Invoice</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transaksi as $trans)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$trans->no_invoice}}</td>
                                        <td>Rp. {{number_format($trans->total_bayar)}}</td>
                                        <td>
                                            @if ($trans->bayar == 1)
                                            <span class="badge badge-soft-success py-1">Done</span>
                                            @else
                                            <span class="badge badge-soft-warning py-1">Pending</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> <!-- end table-responsive-->
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->
        </div>
        <!-- end row -->

    </div>
</div> <!-- content -->

@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('admin/assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('admin/assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.select.min.js') }}"></script>
    
    <!-- Datatables init -->
    <script src="{{ asset('admin/assets/js/pages/datatables.init.js') }}"></script>

    <!-- optional plugins -->
    <script src="{{ asset('admin/assets/libs/moment/moment.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/flatpickr/flatpickr.min.js') }}"></script>

    <!-- page js -->
    <script src="{{ asset('admin/assets/js/pages/dashboard.init.js') }}"></script>
@endsection