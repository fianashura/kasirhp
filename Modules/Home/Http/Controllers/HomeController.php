<?php

namespace Modules\Home\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Master\Entities\Produk;
use Modules\Stok\Entities\Stok;
use Modules\Kasir\Entities\Transaksi;
use Modules\Kasir\Entities\DetailTransaksi;
use Illuminate\Support\Str;
use App\User;
use DB;
use DataTables;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $date = date('Y-m-d');
        $cust = Transaksi::whereDate('created_at',$date)->sum('total_bayar');
        $customer = Transaksi::whereDate('created_at',$date)->count();
        $prod = DetailTransaksi::whereDate('created_at',$date)->sum('jumlah_barang');
        $stok = Stok::sum('jumlah');
        $customer_total = Transaksi::count();
        $produk_total = DetailTransaksi::sum('jumlah_barang');
        $hasil_total = Transaksi::sum('total_bayar');
        $transaksi = Transaksi::orderBy('created_at','DESC')->limit('5')->get();
        $no = 1;
        // dd($cust);
        return view('home::index', compact('cust','customer','prod','stok','customer_total','produk_total','hasil_total','transaksi','no'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('home::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('home::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('home::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
