<?php

namespace Modules\Kasir\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Master\Entities\Produk;
use Modules\Stok\Entities\Stok;
use Modules\Kasir\Entities\Transaksi;
use Modules\Kasir\Entities\DetailTransaksi;
use Illuminate\Support\Str;
use App\User;
use DB;
use DataTables;

class KasirController extends Controller
{
    /**≈
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $no = 1;
        $produk = Stok::leftjoin('produk','stok.id_produk','produk.id_produk')->get();
        $cek = Transaksi::where('bayar','0')->first();
        if($cek != NULL){
            $transaksi = DB::table('detail_transaksi')
                ->leftjoin('transaksi','detail_transaksi.id_transaksi','transaksi.id_transaksi')
                ->leftjoin('stok','detail_transaksi.id_stok','stok.id_stok')
                ->leftjoin('produk','stok.id_produk','produk.id_produk')
                ->where('transaksi.id_transaksi',$cek->id_transaksi)
                ->get();  
        }else{
            $transaksi = [];
        }  
        return view('kasir::index', compact('no','cek','produk','transaksi'));
    }
    
    public function index_transaksi($id_transaksi)
    {
        $produk = Stok::leftjoin('produk','stok.id_produk','produk.id_produk')->get();
        $transaksi = DB::table('detail_transaksi')
            ->leftjoin('transaksi','detail_transaksi.id_transaksi','transaksi.id_transaksi')
            ->leftjoin('stok','detail_transaksi.id_stok','stok.id_stok')
            ->leftjoin('produk','stok.id_produk','produk.id_produk')
            ->where('transaksi.id_transaksi',$id_transaksi)
            ->get();
        return view('kasir::transaksi', compact('produk','transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('kasir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store_transaksi(Request $request)
    {
        $trans = Transaksi::where('bayar','0')->first();
        $stok = Stok::where('id_stok',$request->produk)->first();
        if($stok->jumlah != 0){
            if($trans == NULL){
                $tra = new Transaksi;

                $tra->id_transaksi = Str::upper(Str::uuid());
                $tra->id_pegawai = Auth::user()->person_id;
                $tra->no_invoice = 'INV-'.rand(1000,9999).'-'.date('m').'-'.date('y');
                $tra->tgl_transaksi = date('Ymd');
                $tra->bayar = '0';

                $tra->save();

                
                $detail = new DetailTransaksi;
                $detail->id_detail_transaksi = Str::upper(Str::uuid());
                $detail->id_transaksi = $tra->id_transaksi;
                $detail->id_stok = $request['produk'];
                $detail->jumlah_barang = $request['jumlah'];
        
                $detail->save();
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => 'Data Terinput'
                ]);
            }else{
                $detail = new DetailTransaksi;
                $detail->id_detail_transaksi = Str::upper(Str::uuid());
                $detail->id_transaksi = $trans->id_transaksi;
                $detail->id_stok = $request['produk'];
                $detail->jumlah_barang = $request['jumlah'];

                $detail->save();
                
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => 'Data Terinput'
                ]);
            }
        }else{
            return response()->json([
                'success' => false,
                'type' => 'error',
                'message' => 'Maaf Stok Kosong'
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */

    public function bayar(request $request)
    {
        $cek = Transaksi::where('bayar','0')->first();
        if($cek != NULL){
            DB::table('transaksi')->where('id_transaksi',$cek->id_transaksi)->update([
                'pembayaran' => $request->bayar,
                'total_bayar' => $request->total_bayar,
                'bayar' => '1',
            ]);
            
            $barang = DetailTransaksi::where('id_transaksi',$cek->id_transaksi)->get();
            foreach($barang as $item){
                $stok = Stok::where('id_stok',$item->id_stok)->first();
                $sisa = $stok->jumlah - $item->jumlah_barang;
                DB::table('stok')->where('id_stok',$item->id_stok)->update([
                    'jumlah' => $sisa,
                ]);
            }

            return response()->json([
                'success' => true,
                'message' => 'Pembayaran Berhasil'
            ]); 
        }else{
            return response()->json([
                'success' => true,
                'message' => 'Data Transaksi Belum Ada'
            ]);
        }
    }
    public function show($id)
    {
        return view('kasir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('kasir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id_detail_transaksi)
    {
        $detail = DetailTransaksi::where('id_detail_transaksi',$id_detail_transaksi)->delete();

        // DB::table('tbl_bank')
        // return $bank;
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Di Hapus'
        ]);
    }
    
    public function batal($id_transaksi)
    {
        $transaksi = Transaksi::where('id_transaksi',$id_transaksi)->delete();

        // DB::table('tbl_bank')
        // return $bank;
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Di Hapus'
        ]);
    }
}
