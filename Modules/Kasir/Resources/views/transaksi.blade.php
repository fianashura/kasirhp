@extends('home::layouts.master')

@section('css')
    {{-- <link href="{{ asset('admin/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('admin/assets/assets/sweetalert2/sweetalert2.css') }}"/>
@endsection

@section('content')

    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kasir</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Kasir</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <button onclick="addForm()" type="button" class="btn btn-primary width-md"><i data-feather="plus-circle"></i> Tambah Barang</button><br><br>
                            <div class="table-responsive">
                            <table class="table dt-responsive nowrap table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width:5%">No</th>
                                        <th>Nama Barang</th>
                                        <th style="width:10%">Jumlah</th>
                                        <th style="width:10%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transaksi as $trans)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$trans->nm_produk}}</td>
                                            <td>{{$trans->jumlah}}</td>
                                            <td>
                                                <button onclick="deleteData('{{$trans->id_detail_transaksi}}')" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Data" style="width:40%;"><i data-feather="trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-3 mt-0 header-title">Total Bayar</h4>

                            <table style="width: 100%">
                                <tr>
                                    <td>Total</td>
                                    <td>Total</td>
                                </tr>
                                <tr>
                                    <td>Diskon</td>
                                    <td>Diskon</td>
                                </tr>
                                <tr>
                                    <td>Total Bayar</td>
                                    <td>Total Bayar</td>
                                </tr>
                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-3 mt-0 header-title">Bayar</h4>
                            <form id="basic-form" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }} {{ method_field('POST') }}
                                <input type="hidden" id="id" name="id">
                                <div class="form-group">
                                    {{-- <label for="recipient-name" class="control-label">Bayar</label> --}}
                                    <input type="text" class="form-control" id="bayar" name="bayar" required>
                                </div>
                                
                                <button type="submit" class="btn btn-primary pull-right">Bayar</button>
                            </form>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
    @include('kasir::form')
    

@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('admin/assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('admin/assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/datatables/dataTables.select.min.js') }}"></script>
    
    <!-- Datatables init -->
    <script src="{{ asset('admin/assets/js/pages/datatables.init.js') }}"></script>
    
    <script src="{{ asset('admin/assets/assets/sweetalert2/sweetalert2.min.js') }}"></script>
                
    <script type="text/javascript">
        // var table = $('#data-pegawai').DataTable();
    
        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#modal-form').modal('show');
          $('.modal-title').text('Tambah Data Produk');
          $('#modal-form form')[0].reset();
          
        }
    
        function editForm(id) {
          save_method = 'edit';
          $('input[name=_method]').val('PATCH');
          $('#modal-form form')[0].reset();
          $.ajax({
            url: "{{ url('pegawai') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Data Pegawai');
    
              $('#id').val(data.id_pegawai);
              $('#nm_pegawai').val(data.nm_pegawai);
              $('#jabatan').val(data.jabatan);
              $('#no_hp').val(data.no_hp);
              $('#alamat').val(data.alamat);
            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }
    
        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
          }).then(function () {
              $.ajax({
                  url : "{{ url('pegawai/delete') }}" + '/' + id,
                  type : "POST",
                  data : {'_method' : 'DELETE', '_token' : csrf_token},
                  success : function(data) {
                    window.location.reload();
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        }

    
        $(function(){
              $('#modal-form').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      var id = $('#id').val();
                      if (save_method == 'add') url = "{{ url('kasir/add/') }}";
                      else url = "{{ url('pegawai/update') . '/' }}" + id;
    
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#modal-form form').serialize(),
                          data : new FormData($('#modal-form form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#modal-form').modal('hide');
                              swal({
                                  title: 'Success!',
                                  text: data.message,
                                  type: 'success',
                                  timer: '1500'
                              })
                              window.location.reload();
                          },
                          error : function(){
                              swal({
                                  title: 'Oops...',
                                  text: data.message,
                                  type: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });

      </script>
@endsection