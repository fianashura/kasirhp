<?php

namespace Modules\Kasir\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetailTransaksi extends Model
{
    protected $table = 'detail_transaksi';
    protected $fillable = [];
}
