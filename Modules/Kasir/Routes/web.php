<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('kasir')->group(function() {
    Route::get('/', 'KasirController@index')->name('kasir');
    Route::get('/{id_transaksi}', 'KasirController@index_transaksi')->name('transaksi');
    Route::post('/add', 'KasirController@store_transaksi')->name('kasir_add');
    Route::post('/bayar', 'KasirController@bayar')->name('kasir_bayar');
    Route::get('/delete/{id_detail_transaksi}', 'KasirController@destroy')->name('kasir_delete');
    Route::get('/batal/{id_transaksi}', 'KasirController@batal')->name('batal_delete');
});
